#include<stdio.h>
#include<stdlib.h>
#include "bst.h"

//Initializes binary search tree struct and its members
BStree bstree_ini(int size){ 
	BStree_struct *q=malloc(sizeof(BStree_struct));
	int i=0;
	Node *tree_nodes=malloc((size+1)*sizeof(Node));
	q->is_free=(unsigned char *)malloc((size+1)*sizeof(unsigned char));
	q->tree_nodes=tree_nodes;
	for(i;i<size;i++)
		q->is_free[i]='1';
	q->size=size;
	return q;
}

//Inserts a node into the bst
void bstree_insert(BStree bst,Key *key,int data){
	insert_helper(bst,1,key,data);
}

//Traverse tree in inorder
void bstree_traversal(BStree bst){
	traverse_helper(bst,1);	
}

//Free memory
void bstree_free(BStree bst){
	free(bst->tree_nodes);
	free(bst->is_free);
	free(bst);
}

//Helper method to insert
int insert_helper(BStree bst,int index,Key *key,int data){
	if(index>bst->size)
		return -1;
	if(bst->is_free[index]=='1'){
		bst->tree_nodes[index].key=key;
		bst->tree_nodes[index].data=data;
		bst->is_free[index]='0';
		return 0;
	}
		if(key_comp(*(bst->tree_nodes[index].key),*key)==-1)
			insert_helper(bst,(2*index)+1,key,data);
		else if(key_comp(*(bst->tree_nodes[index].key),*key)==1)
	       		insert_helper(bst,2*index,key,data);
		else{
			printf("Key is already in the tree");
			return -1;
		}
}	

//Helper method to traverse tree
void traverse_helper(BStree bst,int index){
	if(bst->size>=(2*index)){
		if(bst->is_free[2*index]=='0')
			traverse_helper(bst,2*index);
	}
	print_node(bst->tree_nodes[index]);
	if(bst->size>=(2*index)+1){
		if(bst->is_free[2*index+1]=='0')
			traverse_helper(bst,(2*index)+1);
	}
}

int main(void) {
BStree bst;
bst = bstree_ini(1000);
bstree_insert(bst, key_construct("Once", 1), 11);
bstree_insert(bst, key_construct("Upon", 22), 2);
bstree_insert(bst, key_construct("a", 3), 33);
bstree_insert(bst, key_construct("Time", 4), 44);
bstree_insert(bst, key_construct("is", 5), 55);
bstree_insert(bst, key_construct("filmed", 6), 66);
bstree_insert(bst, key_construct("in", 7), 77);
bstree_insert(bst, key_construct("Vancouver", 8), 88);
bstree_insert(bst, key_construct("!", 99), 9);
bstree_insert(bst, key_construct("Once", 5), 50);
bstree_insert(bst, key_construct("Once", 1), 10);
bstree_traversal(bst);
bstree_free(bst);
}








