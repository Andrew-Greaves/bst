#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "data.h"

//Initializes the key construct
Key *key_construct(char *in_name,int in_id){
	Key *k;
	k=(Key *)malloc(sizeof(Key));
	k->name=strdup(in_name);
        k->id=in_id;
	return k;
}

//Compares keys
int key_comp(Key key1,Key key2){
	if(strcmp(key1.name,key2.name)<0)
		return -1;
	else if(strcmp(key1.name,key2.name)>0)
		return 1;
	else if((strcmp(key1.name,key2.name)==0)&&(key1.id==key2.id))
		return 0;
	else if((strcmp(key1.name,key2.name)==0)&&(key1.id<key2.id))
		return -1;
	else if((strcmp(key1.name,key2.name)==0)&&(key1.id>key2.id))
		return 1;
}

//Prints the contents of a key
void print_key(Key *key){
	printf("%s\n",key->name);
	printf("%d\n",key->id);
}

//Prints the contents of a node
void print_node(Node node){
	printf("%s\n",node.key->name);
	printf("%d\n",node.key->id);
	printf("%d\n",node.data);
}


