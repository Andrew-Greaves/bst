#include"data.h"

typedef struct BStree_struct{
	Node *tree_nodes;
	unsigned char *is_free;
	int size;
}BStree_struct;

typedef BStree_struct *BStree;
BStree bstree_ini(int size);
void bstree_insert(BStree bst,Key *key,int data);
void bstree_traversal(BStree bst);
void bstree_free(BStree bst);
int insert_helper(BStree bst,int index,Key *key,int data);
void traverse_helper(BStree bst,int index);
